import re

ALL_TLD = "[a-z]{2,6}"


def alphabetic():
    return re.compile(r"[A-Z]", re.IGNORECASE)


def alphanumeric():
    return re.compile(r"\w")


def bitcoin():
    return re.compile(r"^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$")


def credit_card(card_type="visa"):
    switcher = {
        "amex": r"(?:3[47][0-9]{2}\s?[0-9]{6}\s?[0-9]{5})",
        "discover": r"(?:6(?:011|5[0-9]{2})\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4})",
        "mastercard": r"5[1-5]\d{2}(| |-)(?:\d{4}\1){2}\d{4}",
        "visa": r"4[0-9]{3}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9](?:[0-9]{3})?"
    }

    pattern = switcher.get(card_type)
    return re.compile(f"{pattern}")


def date(date_format="dd/mm"):
    switcher = {
        "dd/mm": lambda: date_day_month(),
        "mm/dd": lambda: date_month_day(),
        "iso": r"([0-9]{4})(-)?(1[0-2]|0[1-9])(?(2)-)(3[01]|0[1-9]|[12][0-9])"
               r"(2[0-3]|[01][0-9])(?(2):)([0-5][0-9])(?(2):)([0-5][0-9])"
    }

    def date_day_month():
        return r"(?:(0?2)/([12][0-9]|0?[1-9])|(0?[469]|11)/(30|[12][0-9]|" \
               r"0?[1-9])|(0?[13578]|1[02])/(3[01]|[12][0-9]|0?[1-9]))/" \
               r"((?:[0-9]{2})?[0-9]{2})"

    def date_month_day():
        return r"(?:([12][0-9]|0?[1-9])/(0?2)|(30|[12][0-9]|0?[1-9])/" \
               r"([469]|11)|(3[01]|[12][0-9]|0?[1-9])/(0?[13578]|1[02]))/" \
               r"((?:[0-9]{2})?[0-9]{2})"

    pattern = switcher.get(date_format)
    return re.compile(f"{pattern}")


def date_and_time():
    return re.compile(r"([0-9]{4})(-)?(1[0-2]|0[1-9])(?(2)-)(3[01]|0[1-9]|"
                      r"[12][0-9]) (2[0-3]|[01][0-9])(?(2):)([0-5][0-9])"
                      r"(?(2):)([0-5][0-9])")


def domain(tld=ALL_TLD):
    pattern = r"((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)"
    return re.compile(f"{pattern}{tld}")


def email(tld=ALL_TLD):
    pattern = r"[\w!#$%&'*+\/=?`{|}~^-]+(?:\.[\w!#$%&'*+\/=?`{|}~^-]+)*@" \
              r"(?:[a-z0-9-]{1,63}+\.)"
    return re.compile(f"{pattern}{tld}")


def hex_color():
    return re.compile(r"#([0-9a-f]{3,6})")


def html_tag():
    return re.compile(r"<([a-z]+)([^<]+)*(?:>(.*)</\1>|\s+/>)")


def ip(protocol="ipv4"):
    switcher = {
        "ipv4": r"^(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}"
                r"(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$",
        "ipv6": r"(?:(?:(?:[A-F0-9]{1,4}:){6}|(?=(?:[A-F0-9]{0,4}:){0,6}"
                r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}$)(([0-9A-F]{1,4}:){0,5}|:)"
                r"((:[0-9A-F]{1,4}){1,5}:|:)|::(?:[A-F0-9]{1,4}:){5})"
                r"(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}"
                r"(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])|"
                r"(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}|(?=(?:[A-F0-9]{0,4}:)"
                r"{0,7}[A-F0-9]{0,4}$)(([0-9A-F]{1,4}:){1,7}|:)((:[0-9A-F]"
                r"{1,4}){1,7}|:)|(?:[A-F0-9]{1,4}:){7}:|:(:[A-F0-9]{1,4}){7})"
    }

    pattern = switcher.get(protocol)
    return re.compile(f"{pattern}", re.IGNORECASE)


def isbn(isbn_format=None):
    switcher = {
        10: r"(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})"
            r"[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]",
        13: r"(?:ISBN(?:-13)?:? )?(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]"
            r"{17}$)97[89][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]"
    }
    isbn_10_or_13 = r"(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+" \
                    r"[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+" \
                    r"[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?" \
                    r"[0-9]+[- ]?[0-9]+[- ]?[0-9X]"

    pattern = switcher.get(isbn_format, isbn_10_or_13)
    return re.compile(f"{pattern}")


def mac():
    return re.compile(r"([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})")


def md5():
    return re.compile(r"\b[A-Fa-f0-9]{32}\b")


def morse_code():
    return re.compile(r"^[.-]{1,5}(?:[ \t]+[.-]{1,5})*(?:[ \t]+[.-]{1,5}"
                      r"(?:[ \t]+[.-]{1,5})*)*")


def phone_number():
    return re.compile(r"[+]?(?:(\d+(?:.\d+)?)|\d+(?:.\d+)?)(?:[ -]?"
                      r"(?:(\d+(?:.\d+)?)|\d+(?:.\d+)?))*(?:[ ]?"
                      r"(?:x|ext).?[ ]?\d{1,5})?")


def postal_code(country="us"):
    switcher = {
        "ad": r"\b(AD)[1-7]\d{2}\b",
        "ar": r"\b[ABCDEFGHJKLMNPQRSTUVWXYZ][1-9]\d{3}[A-Z]{3}\b",
        "be": r"\b(?:(?:[1-9])(?:\d{3}))\b",
        "br": r"\b\d{5}-?\d{3}\b",
        "ca": r"\b(?:[ABCEGHJ-NPRSTVXY]\d[A-Z][ -]?\d[A-Zh]\d)\b",
        "ch": r"\b^[1-9]\d{3}\b",
        "cl": r"\b[1-9]\d{6}\b",
        "cn": r"\b\d{6}\b",
        "de": r"\b\d{5}\b",
        "dk": r"\b(?:[1-24-9]\d{3}|3[0-8]\d{2})\b",
        "es": r"\b(?:0[1-9]|[1-4]\d|5[0-2])\d{3}\b",
        "fi": r"\b\d{5}\b",
        "fr": r"\b(?:[0-8]\d|9[0-8])\d{3}\b",
        "gb": r"(?:GIR 0AA|(?:(?:(?:A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|"
              r"D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|"
              r"JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|"
              r"R[GHM]|S[AEGK-PRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?"
              r"\d|(?:(?:E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|"
              r"(?:SW|W)(?:[2-9]|[1-9]\d)|EC[1-9]\d)\d[ABD-HJLNP-UW-Z]{2}))",
        "ie": r"\b(?:(a(4[125s]|6[37]|7[5s]|[8b][1-6s]|9[12468b])|c1[5s]|"
              r"d([0o][1-9sb]|1[0-8osb]|2[024o]|6w)|e(2[15s]|3[24]|4[15s]|"
              r"[5s]3|91)|f(12|2[368b]|3[15s]|4[25s]|[5s][26]|9[1-4])|"
              r"h(1[2468b]|23|[5s][34]|6[25s]|[79]1)|k(3[246]|4[5s]|[5s]6|67|"
              r"7[8b])|n(3[79]|[49]1)|p(1[247]|2[45s]|3[126]|4[37]|[5s][16]|"
              r"6[17]|7[25s]|[8b][15s])|r(14|21|3[25s]|4[25s]|[5s][16]|9[35s])"
              r"|t(12|23|34|4[5s]|[5s]6)|v(1[45s]|23|3[15s]|42|9[2-5s])|"
              r"w(12|23|34|91)|x(3[5s]|42|91)|y(14|2[15s]|3[45s]))\s?"
              r"[abcdefhknoprtsvwxy\d]{4})\b",
        "it": r"\b\d{5}\b",
        "jp": r"\b\d{3}-\d{4}\b",
        "lu": r"\b\d{4}\b",
        "mx": r"\b\d{4}\b",
        "nl": r"\b\d{4}\s[A-Z]{2}\b",
        "no": r"\b\d{4}\b",
        "pt": r"\b\d{4}-\d{3}\b",
        "ru": r"\b[1-4]|6\d{5}\b",
        "se": r"\b(10|11|20|21|22|25|30|35|39|40|41|50|55|58|60|63|65|70|72|"
              r"75|80|85|90|97)\d\s\d{2}\b",
        "us": r"\b\d{5}(?:-\d{4})?\b"
    }

    pattern = switcher.get(country)
    return re.compile(f"{pattern}", re.IGNORECASE)


def sha(version=256):
    switcher = {
        1: r"\b[A-Fa-f0-9]{7,40}\b",
        256: r"\b[A-Fa-f0-9]{64}\b"
    }

    pattern = switcher.get(version)
    return re.compile(f"{pattern}")


def time(time_format="12", seconds=False):
    switcher = {
        "12": [r"(1[0-2]|0?[1-9]):([0-5]?[0-9])( ?[AP]M)?",
               r"(1[0-2]|0?[1-9]):([0-5]?[0-9]):([0-5]?[0-9])( ?[AP]M)?"],
        "24": [r"(2[0-3]|[01]?[0-9]):([0-5]?[0-9])",
               r"(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])"],
        "iso": [r"(2[0-3]|[01][0-9]):?([0-5][0-9]):?([0-5][0-9])"
                r"(Z|[+-](?:2[0-3]|[01][0-9])(?::?(?:[0-5][0-9]))?)"]
    }

    if time_format in ["12", "24"] and seconds is True:
        pattern = switcher.get(time_format)[1]
    else:
        pattern = switcher.get(time_format)[0]

    return re.compile(f"{pattern}")


def url():
    return re.compile("([a-z][a-z0-9+\-.]*:(\/\/([a-z0-9\-._~%!$&'()*+,;=]"
                      "+@)?([a-z0-9\-._~%]+|\[[a-f0-9:.]+\]|\[v[a-f0-9]"
                      "[a-z0-9\-._~%!$&'()*+,;=:]+\])(:[0-9]+)?"
                      "(\/[a-z0-9\-._~%!$&'()*+,;=:@]+)*\/?|"
                      "(\/?[a-z0-9\-._~%!$&'()*+,;=:@]+"
                      "(\/[a-z0-9\-._~%!$&'()*+,;=:@]+)*\/?)?)|"
                      "([a-z0-9\-._~%!$&'()*+,;=@]+"
                      "(\/[a-z0-9\-._~%!$&'()*+,;=:@]+)*\/?|"
                      "(\/[a-z0-9\-._~%!$&'()*+,;=:@]+)+\/?))"
                      "(\?[a-z0-9\-._~%!$&'()*+,;=:@\/?]*)?"
                      "(#[a-z0-9\-._~%!$&'()*+,;=:@\/?]*)?", re.IGNORECASE)


def uuid():
    return re.compile(r"[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}"
                      r"-[89ab][0-9a-f]{3}-[0-9a-f]{12}")
